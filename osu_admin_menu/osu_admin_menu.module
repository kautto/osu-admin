<?php

/**
 * @file
 * osu_admin_menu module file.
 * By convention we put only hook implementations here.
 */

/**
 * Implements hook_permission().
 */
function osu_admin_menu_permission() {
  return array(
    'administer apps updates' => array(
      'title' => t('Administer the App Store Updates'),
      'description' => t('Update Apps'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function osu_admin_menu_menu() {
  $items = array();

  $items['admin/advanced'] = array(
    'title' => 'Advanced',
    'description' => 'Manage advanced site features (administrators only).',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer osu_menus'),
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
    'weight' => 50,
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function osu_admin_menu_menu_alter(&$items) {

  // Change the weights of principle menu entries.
  $items['admin/content']['weight'] = 5;
  $items['admin/menu']['weight'] = 10;
  $items['admin/apps']['weight'] = 15;
  $items['admin/people']['weight'] = 20;
  $items['admin/reports']['weight'] = 25;
  $items['admin/osu_help']['weight'] = 30;

  // Avoid confusion between people (directory) and user accounts.
  $items['admin/people']['title'] = 'Users';
  $items['admin/people/people']['title'] = 'CMS Users';

  // Avoid confusion between first "Menu" and "Site Navigation"
  $items['admin/menu']['title'] = 'Navigation';


  // Alters the behavior of the links in the admin toolbar
  // such that you don't need to grant "access administration pages"
  // to get things like admin/content or admin/reports.
  // The code was taken from the admin module.
  foreach ($items as $path => $item) {
    // Smarter access callback for poorly checked landing pages.
    if (!empty($item['access arguments']) && !empty($item['page callback']) && $item['access arguments'] === array('access administration pages') && in_array($item['page callback'], array(
        'system_admin_menu_block_page',
        'system_settings_overview',
      ))
    ) {
      $items[$path]['access callback'] = 'osu_admin_menu_landing_page_access';
      $items[$path]['access arguments'] = array($path);
    }
  }

  // Add an extra permission to restrict access to administering updates.
  $items["admin/apps/%apps_server/update"]['access arguments'] = array('administer apps updates');

}

/**
 * Implements hook_navbar_alter().
 */
function osu_admin_menu_navbar_alter(&$items) {
  // Change the label to view an account from view profile to view account.
  $items['user']['tray']['user_links']['#links']['account']['title'] = t('View account');
}


/**
 * Implements hook_flush_caches().
 */
function osu_admin_menu_flush_caches() {

  // Look up the parent id of our advanced menu item.
  $plid = db_select('menu_links', 'ml')
    ->fields('ml', array('mlid'))
    ->condition('menu_name', 'management')
    ->condition('link_path', 'admin/advanced')
    ->execute()
    ->fetchField();

  // Move all of these things under the "Advanced" parent link id.
  // Helps prevent confusion when demoing things as admin.
  $advanced_items = array(
    'admin/panopoly',
    'admin/structure',
    'admin/appearance',
    'admin/modules',
    'admin/config',
    'admin/advanced_help',
  );

  // Allow other modules to add stuff to advanced menu.
  drupal_alter('advanced_menu', $advanced_items);

  foreach ($advanced_items as $path) {
    $mlid = db_select('menu_links', 'ml')
      ->fields('ml', array('mlid'))
      ->condition('menu_name', 'management')
      ->condition('link_path', $path)
      ->execute()
      ->fetchField();

    if ($mlid) {
      $item = menu_link_load($mlid);
      $item['customized'] = 1;
      $item['plid'] = $plid;
      menu_link_save($item);
    }
  }
}

/**
 * Menu access callback for admin landing pages.
 *
 * For a given landing page, grant access if the current user has access
 * to any of its child menu items.
 *
 * @param string $path
 *   landing page path
 *
 * @return mixed
 *   returns access
 */
function osu_admin_menu_landing_page_access($path) {
  static $paths;
  if (!isset($paths[$path])) {
    $paths[$path] = FALSE;

    // Retrieve menu item but without menu_get_item()
    $item = db_select('menu_links')
      ->fields('menu_links')
      ->condition('module', 'system')
      ->condition('router_path', $path)
      ->execute()
      ->fetchAssoc();
    if ($item) {
      $query = db_select('menu_links', 'ml');
      $query->leftJoin('menu_router', 'm', 'm.path = ml.router_path');
      $query->fields('ml');
      $query->fields('m', array_diff(drupal_schema_fields_sql('menu_router'), array('weight')));
      $query->condition('ml.plid', $item['mlid']);
      $result = $query->execute();

      while ($item = $result->fetchAssoc()) {
        _menu_link_translate($item);
        if ($item['access']) {
          $paths[$path] = TRUE;
          break;
        }
      }
    }
  }
  return $paths[$path];
}