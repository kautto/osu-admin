<?php

/**
 * @file
 * osu_admin_perms module file.
 * By convention we put only hook implementations here.
 */

/**
 * Implements of hook_role_perm_set().
 */
function osu_admin_perms_role_perm_set() {

  // This module will deny all permissions to managed roles.
  // This way a managed role only gets access to something if
  // it has been programatically granted.
  $perms = array_keys(module_invoke_all('permission'));

  // Anonymous users.
  $anon = array(
    'access content',
    'view files',
    'search content',
    'use advanced search',
    'create files',
  );

  // Authenticated users.
  $auth = array(
    'view own files',
    'view own private files',
  );
  $auth = array_merge($auth, $anon);

  // Content Contributors.
  $cc = array(
    'view own unpublished content',
    'view revisions',
    'save draft',
    'use token insert',
    'access workbench',
    'access navbar',
    'view the administration theme',
  );
  $cc = array_merge($cc, $auth);

  // Content Authors.
  $ca = array(
    'access navbar',
    'view the administration theme',
  );
  $ca = array_merge($ca, $auth);

  // Content Editors.
  $ce = array(
    'administer nodes',
    'access content overview',
    'view revisions',
    'revert revisions',
    'use panels in place editing',
    'change layouts in place editing',
    "view own unpublished content",
  );
  $ce = array_merge($ce, $ca);

  // Site Managers.
  $sm = array(

    // Delegated user administration -
    // in theory this should be all perms matching standard roles.
    'assign site manager role',
    'assign content editor role',
    'assign content author role',
    'assign content contributor role',
    'administer users',
    'access user profiles',
    'administer apps',
    'administer url aliases',
    'create url aliases',
    'access site reports',
    'make front page',

  );
  $sm = array_merge($sm, $ce);

  $admin = array(
    // Access administration pages includes many permissions and cannot be
    // given out to site managers even though there are some flaky issues
    // around top level menu item visibility.
    'access administration pages',
    'access site in maintenance mode',
    'administer taxonomy',
    'masquerade as user',
    'masquerade as any user',
    'masquerade as admin',
  );
  $admin = array_merge($admin, $sm);

  return array(

    // These permissions will be explicitly set (on or off) for all roles below.
    'permissions' => $perms,
    // Grants define actively managed roles and which of the above permissions
    // they will receive (and by omission not receive).
    'grants' => array(
      'site administrator' => $admin,
      'site manager' => $sm,
      'content editor' => $ce,
      'content author' => $ca,
      'content contributor' => $cc,
      'authenticated user' => $auth,
      'anonymous user' => $anon,
    ),
    // Regex grants apply to any actively managed role matching the pattern.
    // They behave similarly to regular grants but may match actively managed
    // roles defined in other modules. For example, osu_news may define a
    // "news editor" role but osu_admin might define some basic permissions
    // needed by all "blah editor".
    'regex_grants' => array(
      '.* editor' => $ce,
      '.* author' => $ca,
      '.* contributor' => $cc,
    ),
  );
}
