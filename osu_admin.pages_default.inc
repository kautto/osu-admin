<?php
/**
 * @file
 * osu_admin.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function osu_admin_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit_panel_context_2';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Custom Node Edit Page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'css_id' => 'node-edit',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
      'sidebar' => NULL,
    ),
    'sidebar' => array(
      'style' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8695a0c4-0fd5-4a5e-81e0-7e54bc4f6d61';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-db000db8-71b8-4541-88de-d6685abf680c';
    $pane->panel = 'contentmain';
    $pane->type = 'node_form_title';
    $pane->subtype = 'node_form_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'db000db8-71b8-4541-88de-d6685abf680c';
    $display->content['new-db000db8-71b8-4541-88de-d6685abf680c'] = $pane;
    $display->panels['contentmain'][0] = 'new-db000db8-71b8-4541-88de-d6685abf680c';
    $pane = new stdClass();
    $pane->pid = 'new-f7561b4c-bda0-44b0-9ba0-ccafb36772d1';
    $pane->panel = 'contentmain';
    $pane->type = 'node_form_path';
    $pane->subtype = 'node_form_path';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f7561b4c-bda0-44b0-9ba0-ccafb36772d1';
    $display->content['new-f7561b4c-bda0-44b0-9ba0-ccafb36772d1'] = $pane;
    $display->panels['contentmain'][1] = 'new-f7561b4c-bda0-44b0-9ba0-ccafb36772d1';
    $pane = new stdClass();
    $pane->pid = 'new-c75682a9-de8d-4442-a80d-415e31b9ee63';
    $pane->panel = 'contentmain';
    $pane->type = 'form';
    $pane->subtype = 'form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c75682a9-de8d-4442-a80d-415e31b9ee63';
    $display->content['new-c75682a9-de8d-4442-a80d-415e31b9ee63'] = $pane;
    $display->panels['contentmain'][2] = 'new-c75682a9-de8d-4442-a80d-415e31b9ee63';
    $pane = new stdClass();
    $pane->pid = 'new-f8e482d5-5f9c-45d0-af84-761f3a9aeaed';
    $pane->panel = 'sidebar';
    $pane->type = 'panelizer_form_default';
    $pane->subtype = 'panelizer_form_default';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f8e482d5-5f9c-45d0-af84-761f3a9aeaed';
    $display->content['new-f8e482d5-5f9c-45d0-af84-761f3a9aeaed'] = $pane;
    $display->panels['sidebar'][0] = 'new-f8e482d5-5f9c-45d0-af84-761f3a9aeaed';
    $pane = new stdClass();
    $pane->pid = 'new-6d398131-b96d-4f6c-b612-4f6916e628d6';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_menu';
    $pane->subtype = 'node_form_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6d398131-b96d-4f6c-b612-4f6916e628d6';
    $display->content['new-6d398131-b96d-4f6c-b612-4f6916e628d6'] = $pane;
    $display->panels['sidebar'][1] = 'new-6d398131-b96d-4f6c-b612-4f6916e628d6';
    $pane = new stdClass();
    $pane->pid = 'new-fd679876-bf83-4076-bb3d-d953d93bc56a';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_author';
    $pane->subtype = 'node_form_author';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'default',
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'fd679876-bf83-4076-bb3d-d953d93bc56a';
    $display->content['new-fd679876-bf83-4076-bb3d-d953d93bc56a'] = $pane;
    $display->panels['sidebar'][2] = 'new-fd679876-bf83-4076-bb3d-d953d93bc56a';
    $pane = new stdClass();
    $pane->pid = 'new-c1b0df21-0e24-40fa-9bc9-f65a5ecb6f41';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_publishing';
    $pane->subtype = 'node_form_publishing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'c1b0df21-0e24-40fa-9bc9-f65a5ecb6f41';
    $display->content['new-c1b0df21-0e24-40fa-9bc9-f65a5ecb6f41'] = $pane;
    $display->panels['sidebar'][3] = 'new-c1b0df21-0e24-40fa-9bc9-f65a5ecb6f41';
    $pane = new stdClass();
    $pane->pid = 'new-d3b62c9c-7a35-4531-830d-63137aa7f843';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_buttons';
    $pane->subtype = 'node_form_buttons';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(),
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'd3b62c9c-7a35-4531-830d-63137aa7f843';
    $display->content['new-d3b62c9c-7a35-4531-830d-63137aa7f843'] = $pane;
    $display->panels['sidebar'][4] = 'new-d3b62c9c-7a35-4531-830d-63137aa7f843';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-c75682a9-de8d-4442-a80d-415e31b9ee63';
  $handler->conf['display'] = $display;
  $export['node_edit_panel_context_2'] = $handler;

  return $export;
}
