api = 2
core = 7.x

projects[role_delegation][version] = 1.1
projects[role_delegation][subdir] = contrib
projects[role_delegation][patch][2051669] = http://drupal.org/files/2051669-1-role_delegation-views-bulk-operations_0.patch

projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = contrib

projects[filter_perms][version] = 1.0
projects[filter_perms][subdir] = contrib

projects[role_perm][type] = module
projects[role_perm][subdir] = custom
projects[role_perm][download][type] = git
projects[role_perm][download][url] = https://code.osu.edu/openosu/role_perm.git
projects[role_perm][download][branch] = 7.x-1.x

projects[make_front][type] = module
projects[make_front][subdir] = custom
projects[make_front][download][type] = git
projects[make_front][download][url] = https://code.osu.edu/openosu/make_front.git
projects[make_front][download][branch] = 7.x-1.x
