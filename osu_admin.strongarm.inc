<?php
/**
 * @file
 * osu_admin.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function osu_admin_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'masquerade_admin_roles';
  $strongarm->value = array(
    5 => '5',
    1 => 0,
    2 => 0,
    3 => 0,
    4 => 0,
    8 => 0,
    9 => 0,
    7 => 0,
    13 => 0,
    10 => 0,
    12 => 0,
    11 => 0,
    6 => 0,
  );
  $export['masquerade_admin_roles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panopoly_admin_front_page_and_sticky';
  $strongarm->value = '1';
  $export['panopoly_admin_front_page_and_sticky'] = $strongarm;

  return $export;
}
